const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const vehicleTypeSchema = new mongoose.Schema({
    createdAt: Date,
    updatedAt: Date,
    name: {
        type: String,
        required: [true, 'A vehicle type must have a name'],
        cast: false
    }
}, {strict: "throw"}); 
const VehicleType = mongoose.model('VehicleType', vehicleTypeSchema);

module.exports = VehicleType;