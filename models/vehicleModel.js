const mongoose = require('mongoose');

const vehicleSchema = new mongoose.Schema({
    createdAt: Date,
    updatedAt: Date,
    vehicleType: {
        type: String,
        required: [true, 'A vehicle must have a vehicle type'],
        cast: false
    },
    brand: {
        type: String,
        required: [true, 'A vehicle must have a brand'],
        cast: false
    },
    model: {
        type: String,
        required: [true, 'A vehicle must have a model'],
        cast: false
    },
    constructionYear: {
        type: Number,
        required: [true, 'A vehicle must have a construction year'],
        cast: false
    },
    fuelType: {
        type: String,
        enum : ['petrol', 'diesel', 'hybrid', 'electric'],
        required: [true, 'A vehicle must have a fuel type'],
        cast: false
    },
    numberOfSeats:  {
        type: Number,
        required: [true, 'A vehicle must have a number of seats'],
        cast: false
    },
    picture:  {
        type: String,
        required: [true, 'A vehicle must have a picture'],
        cast: false
    },
    pricePerDay:  {
        type: Number,
        required: [true, 'A vehicle must have a price per day'],
        cast: false
    },
    count:  {
        type: Number,
        required: [true, 'A vehicle must have number of available vehicles'],
        cast: false
    },
},{strict: "throw"}); 
const Vehicle = mongoose.model('Vehicle', vehicleSchema);

module.exports = Vehicle;