const mongoose = require('mongoose');

const rentalEventSchema = new mongoose.Schema({
    start: {
        type: Date,
        required: [true, 'A rental event must have a start date'],
        cast: false
    },
    end: {
        type: Date,
        required: [true, 'A rental event must have an end date'],
        cast: false
    },
    price: Number,
    vehicle: {
        type: String,
        required: [true, 'A rental event must have a vehicle'],
        cast: false
    },
    customer: {
        type: String,
        required: [true, 'A rental event must have a customer'],
        cast: false
    }
}, {strict: "throw"}); 
const RentalEvent = mongoose.model('RentalEvent', rentalEventSchema);

module.exports = RentalEvent;