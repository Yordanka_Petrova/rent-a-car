const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
    createdAt: Date,
    updatedAt: Date,
    fullName:  {
        type: String,
        required: [true, 'A customer must have a full name'],
        cast: false
    },
    email: {
        type: String,
        required: [true, 'A customer must have an email'],
        cast: false
    },
    phone: {
        type: String,
        required: [true, 'A customer must have a phone number'],
        cast: false
    },
}, {strict: "throw"}); 
const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;