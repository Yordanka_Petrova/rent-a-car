const express = require('express');
const mongoose = require('mongoose');

const vehicleTypeRouter = require('./routes/vehicleTypeRoutes');
const vehicleRouter = require('./routes/vehicleRoutes');
const customerRouter = require('./routes/customerRoutes');
const rentalEventRouter = require('./routes/rentalEventRoutes');
const userRouter = require('./routes/userRoutes');

mongoose.set('useCreateIndex', true);
mongoose.connect("mongodb://localhost:27017/rentacar", {useNewUrlParser: true, useUnifiedTopology: true});

const app = express();
app.use(express.json());

app.use('/vehicle-type', vehicleTypeRouter); 
app.use('/vehicle', vehicleRouter); 
app.use('/customer', customerRouter); 
app.use('/rental-event', rentalEventRouter); 
app.use('/users', userRouter); 

const port = 3000;
app.listen(port, () => {
    console.log(`App running on port ${port}...`);
})