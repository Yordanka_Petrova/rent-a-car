const express = require('express');
const customerController = require('./../controllers/customerController');
const authController = require('../controllers/authController');
const router = express.Router();

router.use(authController.protect);

router
    .route('/')
    .post(customerController.createCustomer);

router
    .route('/:id')
    .delete(customerController.deleteCustomer);

router
    .route('/')
    .get(customerController.getCustomer);

router
    .route('/:id')
    .patch(customerController.updateCustomer);

module.exports = router;