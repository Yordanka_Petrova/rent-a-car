const express = require('express');
const vehicleController = require('./../controllers/vehicleController');
const authController = require('../controllers/authController');
const router = express.Router();

router.use(authController.protect);

router
    .route('/')
    .post(vehicleController.createVehile);

router
    .route('/:id')
    .delete(vehicleController.deleteVehicle);

router
    .route('/')
    .get(vehicleController.getVehicle);

router
    .route('/:id')
    .patch(vehicleController.updateVehicle);

module.exports = router;