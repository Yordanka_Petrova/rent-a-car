const express = require('express');
const vehicleTypeController = require('./../controllers/vehicleTypeController');
const authController = require('../controllers/authController');
const router = express.Router();

router.use(authController.protect);

router
    .route('/')
    .post(vehicleTypeController.createVehicleType);

router
    .route('/:id')
    .delete(vehicleTypeController.deleteVehicleType);

router
    .route('/')
    .get(vehicleTypeController.getVehicleType);

router
    .route('/:id')
    .patch(vehicleTypeController.updateVehicleType);

module.exports = router;

