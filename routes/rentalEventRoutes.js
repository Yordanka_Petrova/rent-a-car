const express = require('express');
const rentalEventController = require('./../controllers/rentalEventController');
const authController = require('../controllers/authController');
const router = express.Router();

router.use(authController.protect);

router
    .route('/')
    .post(rentalEventController.createRentalEvent);

router
    .route('/:id')
    .delete(rentalEventController.deleteRentalEvent);

router
    .route('/')
    .get(rentalEventController.getRentalEvent);

router
    .route('/:id')
    .patch(rentalEventController.updateRentalEvent);

module.exports = router;