const VehicleType = require('../models/vehicleTypeModel');

exports.createVehicleType = async (req, res) => {
    try{
        const vehicleType = new VehicleType({ 
                createdAt: Date.now(),
                updatedAt: Date.now(), 
                name: req.body.name
        });
        await vehicleType.save();
        res.send("Successfully created vehicle type");
    }
    catch(err){
        res.status(400).send("Failed to create vehicle type");
    }
};

exports.deleteVehicleType = async (req, res) => {
    try{
        await VehicleType.findByIdAndDelete(req.params.id);

        res.send("Successfully deleted vehicle type");
    }
    catch(err){
        res.status(400).send("Failed to delete vehicle type");
    }
};

exports.getVehicleType = async (req, res) => {
    const vehicleTypes = await VehicleType.find();
    res.send(vehicleTypes);
};

exports.updateVehicleType = async (req, res) => {
    try{
        req.body.updatedAt = Date.now();
        await VehicleType.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true});
        res.send("Successfully updated vehicle type");
    }
    catch(err){
        res.status(400).send("Failed to update vehicle type");
    }
};