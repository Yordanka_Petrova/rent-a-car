const User = require('../models/userModel');

exports.createUser = async (req, res) => {
    
};

exports.deleteUser = async (req, res) => {
    try{
        await User.findByIdAndDelete(req.params.id);

        res.send("Successfully deleted user");
    }
    catch(err){
        res.status(400).send("Failed to delete user");
    }
};

exports.getAllUsers = async (req, res) => {
    const users = await User.find();
    res.send(users);
};

exports.updateUser = async (req, res) => {
    
};