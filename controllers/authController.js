const {promisify} = require('util');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const secret = 'some-very-very-very-long-secret';

const signToken = id => {
    return jwt.sign({id: id}, secret, {
        expiresIn: '1d' 
    });
}

exports.signup = async (req, res) => {
    try{
        const newUser = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            passwordConfirm: req.body.passwordConfirm
        });
        res.status(200).send("Successfully created user");
        
    }
    catch(err){
        res.status(400).send("Failed to create user");
    }
};

exports.login = async (req, res) => {
    try{
        const email = req.body.email;
        const password = req.body.password;

        if(!email || !password) {
           return res.status(400).send("Provide email and password");
        }

        const user = await User.findOne({email: email}).select('+password');

        if(!user || !(await user.correctPassword(password, user.password))) {
            return res.status(401).send("Incorrect email or password");
        }

        const token = signToken(user._id);
        res.status(200).json({
            status: 'success',
            token
        });
    }
    catch(err){
        res.status(400).send("Failed to login");
    }
};

exports.protect = async (req, res, next) => {
    try {
        //getting token and check if it's there
        let token;
        if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
            token = req.headers.authorization.split(' ')[1];
        }
        if(!token){
            return res.status(401).send("You are not logged in! Log in to get access");
        }
        
        //verification token
        const decoded = await promisify(jwt.verify)(token, secret);

        //check if user still exists
        const freshUser = await User.findById(decoded.id);

        if(!freshUser){
            return res.status(401).send("The user belonging to this token does no longer exist");
        }

        req.user = freshUser;
        next();
    }
    catch(err){
        let message;
        if(err.name === 'JsonWebTokenError'){
            message = "Invalid token! Log in again";
        }
        if(err.name === 'TokenExpiredError'){
            message = "Your token has expired! Log in again";
        }
        return res.status(401).send(message);
    }
};
