const RentalEvent = require('../models/rentalEventModel');
const Vehicle = require('../models/vehicleModel');

async function isVip(customer, id) { 
    let today;
    if(!id) {
        today = new Date();
    }
    else {
        const todayEvent = await RentalEvent.findById(id);
        today = todayEvent.start;
    }
    
    const before60Days = new Date(today);
    before60Days.setDate(today.getDate() - 60);
    const rentalEvents = await RentalEvent.find({customer: customer, start: {$gt: before60Days, $lt: today}});
    const numberOfRents = rentalEvents.length;

    return numberOfRents > 3 ? true : false;
}

async function checkDiscount(rentalEvent, id) {
    const vehicle = await Vehicle.findById(rentalEvent.vehicle);
    const price = vehicle.pricePerDay;

    const pricePerMinute = price / (24 * 60);
    const start = new Date(rentalEvent.start);
    const end = new Date(rentalEvent.end);
    const differenceInMinutes = (end - start) / (60 * 1000);
    
    const cost = pricePerMinute * differenceInMinutes;
    const days = Math.floor(differenceInMinutes / (60 * 24));
    
    let discount = 0;
    if (await isVip(rentalEvent.customer, id)) {
        discount = 15;
    } 
    else if (days >= 10) {
        discount = 10;
    } 
    else if (days >= 5) {
        discount = 7;
    } 
    else if (days >= 3) {
        discount = 5;
    }
    const finalPrice = cost - (cost * discount) / 100;
    return finalPrice.toFixed(2);
}

exports.createRentalEvent = async (req, res) => {
    try{
        const rentalEvent = new RentalEvent({
                start: new Date(req.body.start),
                end: new Date(req.body.end),
                price: await checkDiscount(req.body, null),
                vehicle: req.body.vehicle,
                customer: req.body.customer
        });
    
        await rentalEvent.save();
        res.send("Successfully created rental event");
    }
    catch(err){
        res.status(400).send(err);
    }
};

exports.deleteRentalEvent =  async (req, res) => {
    try{
        await RentalEvent.findByIdAndDelete(req.params.id);

        res.send("Successfully deleted rental event");
    }
    catch(err){
        res.status(400).send("Failed to delete rental event");
    }
};

exports.getRentalEvent = async (req, res) => {
    const rentalEvents = await RentalEvent.find();
    res.send(rentalEvents);
};

exports.updateRentalEvent = async (req, res) => {
    try{
        req.body.start = new Date(req.body.start);
        req.body.end = new Date(req.body.end);
        await RentalEvent.findByIdAndUpdate(req.params.id, req.body, {runValidators: true});
        const rentalEvent = await RentalEvent.findById(req.params.id);
        rentalEvent.price = await checkDiscount(rentalEvent, req.params.id);
        await RentalEvent.findByIdAndUpdate(req.params.id, rentalEvent, {new: true, runValidators: true});

        res.send("Successfully updated rental event");
    }
    catch(err){
        res.status(400).send("Failed to update rental event");
    }
};
