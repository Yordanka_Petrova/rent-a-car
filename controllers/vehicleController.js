const Vehicle = require('../models/vehicleModel');

exports.createVehile = async (req, res) => {
    try{
        const vehicle = new Vehicle({
                createdAt: Date.now(),
                updatedAt: Date.now(), 
                vehicleType: req.body.vehicleType,
                brand: req.body.brand,
                model: req.body.model,
                constructionYear: req.body.constructionYear,
                fuelType: req.body.fuelType,
                numberOfSeats: req.body.numberOfSeats,
                picture: req.body.picture,
                pricePerDay: req.body.pricePerDay,
                count: req.body.count
            });
        await vehicle.save();
        res.send("Successfully created vehicle");
    }
    catch(err){
        res.status(400).send("Failed to create vehicle");
    }
};

exports.deleteVehicle = async (req, res) => {
    try{
        await Vehicle.findByIdAndDelete(req.params.id);

        res.send("Successfully deleted vehicle");
    }
    catch(err){
        res.status(400).send("Failed to delete vehicle");
    }
};

exports.getVehicle = async (req, res) => {
    const vehicles = await Vehicle.find();
    res.send(vehicles);
};

exports.updateVehicle = async (req, res) => {
    try{
        req.body.updatedAt = Date.now();
        await Vehicle.findByIdAndUpdate(req.params.id, req.body, {runValidators: true});
        res.send("Successfully updated vehicle");
    }
    catch(err){
        res.status(400).send("Failed to update vehicle");
    }
};
