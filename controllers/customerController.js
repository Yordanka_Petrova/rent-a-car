const Customer = require('../models/customerModel');

exports.createCustomer = async (req, res) => {
    try{
        const customer = new Customer({
                createdAt: Date.now(),
                updatedAt: Date.now(), 
                fullName: req.body.fullName,
                email: req.body.email,
                phone: req.body.phone
            });
        await customer.save();
        res.send("Successfully created customer");
    }
    catch(err){
        res.status(400).send("Failed to create customer");
    }
};

exports.deleteCustomer = async (req, res) => {
    try{
        await Customer.findByIdAndDelete(req.params.id);

        res.send("Successfully deleted customer");
    }
    catch(err){
        res.status(400).send("Failed to delete customer");
    }
};

exports.getCustomer = async (req, res) => {
    const customers = await Customer.find();
    res.send(customers);
};

exports.updateCustomer = async (req, res) => {
    try{
        req.body.updatedAt = Date.now();
        await Customer.findByIdAndUpdate(req.params.id, req.body, {runValidators: true});

        res.send("Successfully updated customer");
    }
    catch(err){
        res.status(400).send("Failed to update customer");
    }
};
